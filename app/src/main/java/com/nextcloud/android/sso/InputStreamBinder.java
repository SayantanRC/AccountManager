/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.nextcloud.android.sso;


import static com.nextcloud.android.sso.Constants.DELIMITER;
import static com.nextcloud.android.sso.Constants.EXCEPTION_ACCOUNT_NOT_FOUND;
import static com.nextcloud.android.sso.Constants.EXCEPTION_HTTP_REQUEST_FAILED;
import static com.nextcloud.android.sso.Constants.EXCEPTION_INVALID_REQUEST_URL;
import static com.nextcloud.android.sso.Constants.EXCEPTION_INVALID_TOKEN;
import static com.nextcloud.android.sso.Constants.EXCEPTION_UNSUPPORTED_METHOD;
import static com.nextcloud.android.sso.Constants.SSO_SHARED_PREFERENCE;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.nextcloud.android.sso.aidl.IInputStreamService;
import com.nextcloud.android.sso.aidl.NextcloudRequest;
import com.nextcloud.android.sso.aidl.ParcelFileDescriptorUtil;
import com.nextcloud.android.utils.AccountManagerUtils;
import com.nextcloud.android.utils.EncryptionUtils;
import com.owncloud.android.lib.common.OwnCloudAccount;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientManager;
import com.owncloud.android.lib.common.OwnCloudClientManagerFactory;
import com.owncloud.android.lib.common.operations.RemoteOperation;

import org.apache.commons.httpclient.HttpConnection;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import at.bitfire.davdroid.BuildConfig;
import at.bitfire.davdroid.log.Logger;

public class InputStreamBinder extends IInputStreamService.Stub {
    private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    private static final String CHARSET_UTF8 = "UTF-8";

    private static final int HTTP_STATUS_CODE_OK = 200;
    private static final int HTTP_STATUS_CODE_MULTIPLE_CHOICES = 300;

    private static final char PATH_SEPARATOR = '/';
    private static final int ZERO_LENGTH = 0;
    private final Context context;

    public InputStreamBinder(Context context) {
        this.context = context;
    }

    public ParcelFileDescriptor performNextcloudRequestV2(ParcelFileDescriptor input) {
        return performNextcloudRequestAndBodyStreamV2(input, null);
    }

    public ParcelFileDescriptor performNextcloudRequestAndBodyStreamV2(
            ParcelFileDescriptor input,
            ParcelFileDescriptor requestBodyParcelFileDescriptor) {
        // read the input
        final InputStream is = new ParcelFileDescriptor.AutoCloseInputStream(input);

        final InputStream requestBodyInputStream = requestBodyParcelFileDescriptor != null ?
                new ParcelFileDescriptor.AutoCloseInputStream(requestBodyParcelFileDescriptor) : null;
        Exception exception = null;
        Response response = new Response();

        try {
            // Start request and catch exceptions
            NextcloudRequest request = deserializeObjectAndCloseStream(is);
            response = processRequestV2(request, requestBodyInputStream);
        } catch (Exception e) {
            Logger.INSTANCE.getLog().log(Level.SEVERE, "Error during Nextcloud request", e);
            exception = e;
        }

        try {
            // Write exception to the stream followed by the actual network stream
            InputStream exceptionStream = serializeObjectToInputStreamV2(exception, response.getPlainHeadersString());
            InputStream resultStream = new java.io.SequenceInputStream(exceptionStream, response.getBody());

            return ParcelFileDescriptorUtil.pipeFrom(resultStream,
                    thread -> Logger.INSTANCE.getLog().log(Level.INFO, "InputStreamBinder: Done sending result"),
                    response.getMethod());
        } catch (IOException e) {
            Logger.INSTANCE.getLog().log(Level.SEVERE, "Error while sending response back to client app", e);
        }

        return null;
    }

    public ParcelFileDescriptor performNextcloudRequest(ParcelFileDescriptor input) {
        return performNextcloudRequestAndBodyStream(input, null);
    }

    public ParcelFileDescriptor performNextcloudRequestAndBodyStream(
            ParcelFileDescriptor input,
            ParcelFileDescriptor requestBodyParcelFileDescriptor) {
        // read the input
        final InputStream is = new ParcelFileDescriptor.AutoCloseInputStream(input);

        final InputStream requestBodyInputStream = requestBodyParcelFileDescriptor != null ?
                new ParcelFileDescriptor.AutoCloseInputStream(requestBodyParcelFileDescriptor) : null;
        Exception exception = null;
        HttpMethodBase httpMethod = null;
        InputStream httpStream = new InputStream() {
            @Override
            public int read() {
                return ZERO_LENGTH;
            }
        };

        try {
            // Start request and catch exceptions
            NextcloudRequest request = deserializeObjectAndCloseStream(is);
            httpMethod = processRequest(request, requestBodyInputStream);
            httpStream = httpMethod.getResponseBodyAsStream();
        } catch (Exception e) {
            Logger.INSTANCE.getLog().log(Level.SEVERE, "Error during Nextcloud request", e);
            exception = e;
        }

        try {
            // Write exception to the stream followed by the actual network stream
            InputStream exceptionStream = serializeObjectToInputStream(exception);
            InputStream resultStream;
            if (httpStream != null) {
                resultStream = new java.io.SequenceInputStream(exceptionStream, httpStream);
            } else {
                resultStream = exceptionStream;
            }

            return ParcelFileDescriptorUtil.pipeFrom(resultStream,
                    thread -> Logger.INSTANCE.getLog().log(Level.INFO, "InputStreamBinder: Done sending result"),
                    httpMethod);
        } catch (IOException e) {
            Logger.INSTANCE.getLog().log(Level.SEVERE, "Error while sending response back to client app", e);
        }

        return null;
    }

    private ByteArrayInputStream serializeObjectToInputStreamV2(Exception exception, String headers) {
        byte[] baosByteArray = new byte[0];
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(exception);
            oos.writeObject(headers);
            oos.flush();
            oos.close();

            baosByteArray = baos.toByteArray();
        } catch (IOException e) {
            Logger.INSTANCE.getLog().log(Level.SEVERE, "Error while sending response back to client app", e);
        }

        return new ByteArrayInputStream(baosByteArray);
    }

    private <T extends Serializable> ByteArrayInputStream serializeObjectToInputStream(T obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(obj);
        oos.flush();
        oos.close();
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private <T extends Serializable> T deserializeObjectAndCloseStream(InputStream is) throws IOException,
            ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(is);
        T result = (T) ois.readObject();
        is.close();
        ois.close();
        return result;
    }

    public class NCPropFindMethod extends PropFindMethod {
        NCPropFindMethod(String uri, int propfindType, int depth) throws IOException {
            super(uri, propfindType, new DavPropertyNameSet(), depth);
        }

        @Override
        protected void processResponseBody(HttpState httpState, HttpConnection httpConnection) {
            // Do not process the response body here. Instead pass it on to client app.
        }
    }

    private HttpMethodBase buildMethod(NextcloudRequest request, Uri baseUri, InputStream requestBodyInputStream)
            throws IOException {
        String requestUrl = baseUri + request.getUrl();
        HttpMethodBase method;
        switch (request.getMethod()) {
            case "GET":
                method = new GetMethod(requestUrl);
                break;

            case "POST":
                method = new PostMethod(requestUrl);
                if (requestBodyInputStream != null) {
                    RequestEntity requestEntity = new InputStreamRequestEntity(requestBodyInputStream);
                    ((PostMethod) method).setRequestEntity(requestEntity);
                } else if (request.getRequestBody() != null) {
                    StringRequestEntity requestEntity = new StringRequestEntity(
                            request.getRequestBody(),
                            CONTENT_TYPE_APPLICATION_JSON,
                            CHARSET_UTF8);
                    ((PostMethod) method).setRequestEntity(requestEntity);
                }
                break;

            case "PATCH":
                method = new PatchMethod(requestUrl);
                if (requestBodyInputStream != null) {
                    RequestEntity requestEntity = new InputStreamRequestEntity(requestBodyInputStream);
                    ((PatchMethod) method).setRequestEntity(requestEntity);
                } else if (request.getRequestBody() != null) {
                    StringRequestEntity requestEntity = new StringRequestEntity(
                            request.getRequestBody(),
                            CONTENT_TYPE_APPLICATION_JSON,
                            CHARSET_UTF8);
                    ((PatchMethod) method).setRequestEntity(requestEntity);
                }
                break;

            case "PUT":
                method = new PutMethod(requestUrl);
                if (requestBodyInputStream != null) {
                    RequestEntity requestEntity = new InputStreamRequestEntity(requestBodyInputStream);
                    ((PutMethod) method).setRequestEntity(requestEntity);
                } else if (request.getRequestBody() != null) {
                    StringRequestEntity requestEntity = new StringRequestEntity(
                            request.getRequestBody(),
                            CONTENT_TYPE_APPLICATION_JSON,
                            CHARSET_UTF8);
                    ((PutMethod) method).setRequestEntity(requestEntity);
                }
                break;

            case "DELETE":
                method = new DeleteMethod(requestUrl);
                break;

            case "PROPFIND":
                method = new NCPropFindMethod(requestUrl, DavConstants.PROPFIND_ALL_PROP, DavConstants.DEPTH_1);
                if (request.getRequestBody() != null) {
                    //text/xml; charset=UTF-8 is taken from XmlRequestEntity... Should be application/xml
                    StringRequestEntity requestEntity = new StringRequestEntity(
                            request.getRequestBody(),
                            "text/xml; charset=UTF-8",
                            CHARSET_UTF8);
                    ((PropFindMethod) method).setRequestEntity(requestEntity);
                }
                break;

            case "MKCOL":
                method = new MkColMethod(requestUrl);
                break;

            case "HEAD":
                method = new HeadMethod(requestUrl);
                break;

            default:
                throw new UnsupportedOperationException(EXCEPTION_UNSUPPORTED_METHOD);

        }
        return method;
    }

    private HttpMethodBase processRequest(final NextcloudRequest request, final InputStream requestBodyInputStream)
            throws UnsupportedOperationException,
            com.owncloud.android.lib.common.accounts.AccountUtils.AccountNotFoundException,
            OperationCanceledException, AuthenticatorException, IOException {
        Account account = AccountManagerUtils.getAccountByName(context, request.getAccountName());
        if (account == null) {
            throw new IllegalStateException(EXCEPTION_ACCOUNT_NOT_FOUND);
        }

        if (!isValid(request)) {
            throw new IllegalStateException(EXCEPTION_INVALID_TOKEN);
        }

        // Validate URL
        if (request.getUrl().isEmpty() || request.getUrl().charAt(0) != PATH_SEPARATOR) {
            throw new IllegalStateException(EXCEPTION_INVALID_REQUEST_URL,
                    new IllegalStateException("URL need to start with a /"));
        }

        final OwnCloudClientManager ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton();
        final OwnCloudAccount ownCloudAccount = new OwnCloudAccount(account, context);
        final OwnCloudClient client = ownCloudClientManager.getClientFor(ownCloudAccount, context);

        HttpMethodBase method = buildMethod(request, client.getBaseUri(), requestBodyInputStream);

        if (request.getParameterV2() != null && !request.getParameterV2().isEmpty()) {
            method.setQueryString(convertListToNVP(request.getParameterV2()));
        } else {
            method.setQueryString(convertMapToNVP(request.getParameter()));
        }

        for (Map.Entry<String, List<String>> header : request.getHeader().entrySet()) {
            // https://stackoverflow.com/a/3097052
            method.addRequestHeader(header.getKey(), TextUtils.join(",", header.getValue()));
        }

        method.setRequestHeader(
                RemoteOperation.OCS_API_HEADER,
                RemoteOperation.OCS_API_HEADER_VALUE
        );

        if (shouldAddHeaderForOidcLogin(context, account, request.getUrl())) {
            method.setRequestHeader(
                    RemoteOperation.OIDC_LOGIN_WITH_TOKEN,
                    RemoteOperation.OIDC_LOGIN_WITH_TOKEN_VALUE
            );
        }

        client.setFollowRedirects(true);
        int status = client.executeMethod(method);

        ownCloudClientManager.saveAllClients(context, account.type);

        // Check if status code is 2xx --> https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#2xx_Success
        if (status >= HTTP_STATUS_CODE_OK && status < HTTP_STATUS_CODE_MULTIPLE_CHOICES) {
            return method;
        } else {
            InputStream inputStream = method.getResponseBodyAsStream();
            String total = "No response body";

            // If response body is available
            if (inputStream != null) {
                total = inputStreamToString(inputStream);
                Logger.INSTANCE.getLog().log(Level.INFO, "InputStreamBinder: " + total);
            }

            method.releaseConnection();
            throw new IllegalStateException(EXCEPTION_HTTP_REQUEST_FAILED,
                    new IllegalStateException(String.valueOf(status),
                            new IllegalStateException(total)));
        }
    }

    /*
    * for non ocs/dav requests (nextcloud app: ex: notes app), when OIDC is used, we need to pass an special header.
    * We should not pass this header for ocs/dav requests as it can cause session cookie not being used for those request.
    *
    * These nextcloud app request paths contain `/index.php/apps/` on them.
     */
    private boolean shouldAddHeaderForOidcLogin(@NonNull Context context, @NonNull Account account, @NonNull String path) {
        boolean isOidcAccount = AccountManagerUtils.isOidcAccount(context, account);
        if (!isOidcAccount) {
            return false;
        }

        return path.contains("/index.php/apps/");
    }

    private Response processRequestV2(final NextcloudRequest request, final InputStream requestBodyInputStream)
            throws UnsupportedOperationException,
            com.owncloud.android.lib.common.accounts.AccountUtils.AccountNotFoundException,
            OperationCanceledException, AuthenticatorException, IOException {
        Account account = AccountManagerUtils.getAccountByName(context, request.getAccountName());
        if (account == null) {
            throw new IllegalStateException(EXCEPTION_ACCOUNT_NOT_FOUND);
        }

        // Validate token
        if (!isValid(request)) {
            throw new IllegalStateException(EXCEPTION_INVALID_TOKEN);
        }

        // Validate URL
        if (request.getUrl().isEmpty() || request.getUrl().charAt(0) != PATH_SEPARATOR) {
            throw new IllegalStateException(EXCEPTION_INVALID_REQUEST_URL,
                    new IllegalStateException("URL need to start with a /"));
        }

        final OwnCloudClientManager ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton();
        final OwnCloudAccount ownCloudAccount = new OwnCloudAccount(account, context);
        final OwnCloudClient client = ownCloudClientManager.getClientFor(ownCloudAccount, context);

        HttpMethodBase method = buildMethod(request, client.getBaseUri(), requestBodyInputStream);

        if (request.getParameterV2() != null && !request.getParameterV2().isEmpty()) {
            method.setQueryString(convertListToNVP(request.getParameterV2()));
        } else {
            method.setQueryString(convertMapToNVP(request.getParameter()));
        }

        for (Map.Entry<String, List<String>> header : request.getHeader().entrySet()) {
            // https://stackoverflow.com/a/3097052
            method.addRequestHeader(header.getKey(), TextUtils.join(",", header.getValue()));
        }

        method.setRequestHeader(
                RemoteOperation.OCS_API_HEADER,
                RemoteOperation.OCS_API_HEADER_VALUE
        );

        if (shouldAddHeaderForOidcLogin(context, account, request.getUrl())) {
            method.setRequestHeader(
                    RemoteOperation.OIDC_LOGIN_WITH_TOKEN,
                    RemoteOperation.OIDC_LOGIN_WITH_TOKEN_VALUE
            );
        }

        client.setFollowRedirects(true);
        int status = client.executeMethod(method);

        ownCloudClientManager.saveAllClients(context, account.type);

        // Check if status code is 2xx --> https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#2xx_Success
        if (status >= HTTP_STATUS_CODE_OK && status < HTTP_STATUS_CODE_MULTIPLE_CHOICES) {
            return new Response(method);
        } else {
            InputStream inputStream = method.getResponseBodyAsStream();
            String total = "No response body";

            // If response body is available
            if (inputStream != null) {
                total = inputStreamToString(inputStream);
            }

            method.releaseConnection();
            throw new IllegalStateException(EXCEPTION_HTTP_REQUEST_FAILED,
                    new IllegalStateException(String.valueOf(status),
                            new IllegalStateException(total)));
        }
    }

    private boolean isValid(NextcloudRequest request) {
        String callingPackageName = context.getPackageManager().getNameForUid(Binder.getCallingUid());

        SharedPreferences sharedPreferences = context.getSharedPreferences(SSO_SHARED_PREFERENCE,
                Context.MODE_PRIVATE);
        String hash = sharedPreferences.getString(callingPackageName + DELIMITER + request.getAccountName(), "");
        return validateToken(hash, request.getToken());
    }

    private boolean validateToken(String hash, String token) {
        if (hash.isEmpty() || !hash.contains("$")) {
            throw new IllegalStateException(EXCEPTION_INVALID_TOKEN);
        }

        String salt = hash.split("\\$")[1]; // TODO extract "$"

        String newHash = EncryptionUtils.generateSHA512(token, salt);

        // As discussed with Lukas R. at the Nextcloud Conf 2018, always compare whole strings
        // and don't exit prematurely if the string does not match anymore to prevent timing-attacks
        return isEqual(hash.getBytes(), newHash.getBytes());
    }

    // Taken from http://codahale.com/a-lesson-in-timing-attacks/
    private static boolean isEqual(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }

        int result = 0;
        for (int i = 0; i < a.length; i++) {
            result |= a[i] ^ b[i];
        }
        return result == 0;
    }

    private static String inputStreamToString(InputStream inputStream) {
        try {
            StringBuilder total = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine();
            while (line != null) {
                total.append(line).append('\n');
                line = reader.readLine();
            }
            return total.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @VisibleForTesting
    public static NameValuePair[] convertMapToNVP(Map<String, String> map) {
        NameValuePair[] nvp = new NameValuePair[map.size()];
        int i = 0;
        for (String key : map.keySet()) {
            nvp[i] = new NameValuePair(key, map.get(key));
            i++;
        }
        return nvp;
    }

    @VisibleForTesting
    public static NameValuePair[] convertListToNVP(Collection<QueryParam> list) {
        NameValuePair[] nvp = new NameValuePair[list.size()];
        int i = 0;
        for (QueryParam pair : list) {
            nvp[i] = new NameValuePair(pair.key, pair.value);
            i++;
        }
        return nvp;
    }
}
