/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.owncloud.android.ui.activity

import android.accounts.Account
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import at.bitfire.davdroid.R
import com.nextcloud.android.sso.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SsoGrantPermissionActivity : AppCompatActivity() {

    private val viewModel: SsoGrantPermissionViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sso_grant_permission)

        lifecycleScope.launch {
            viewModel.permissionEvent
                .flowWithLifecycle(
                    lifecycle = lifecycle,
                    minActiveState = Lifecycle.State.CREATED
                ).collectLatest {
                    when (it) {
                        is SsoGrantPermissionEvent.PermissionGranted -> setSuccessResult(it.bundle)
                        is SsoGrantPermissionEvent.PermissionDenied -> setCanceledResult(it.errorMessage)
                    }
                }
        }

        validateAccount()
    }

    private fun validateAccount() {
        val account: Account? =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                intent.getParcelableExtra(Constants.NEXTCLOUD_FILES_ACCOUNT, Account::class.java)
            } else {
                intent.getParcelableExtra(Constants.NEXTCLOUD_FILES_ACCOUNT)
            }

        viewModel.initValidation(
            callingActivity = callingActivity,
            account = account
        )
    }

    private fun setCanceledResult(exception: String) {
        val data = Intent()
        data.putExtra(Constants.NEXTCLOUD_SSO_EXCEPTION, exception)
        setResult(RESULT_CANCELED, data)
        finish()
    }

    private fun setSuccessResult(result: Bundle) {
        val data = Intent()
        data.putExtra(Constants.NEXTCLOUD_SSO, result)
        setResult(RESULT_OK, data)
        finish()
    }
}
