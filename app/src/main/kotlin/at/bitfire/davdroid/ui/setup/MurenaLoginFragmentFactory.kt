/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import at.bitfire.davdroid.R
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntKey
import dagger.multibindings.IntoMap
import javax.inject.Inject

class MurenaLoginFragmentFactory @Inject constructor(@ApplicationContext val context: Context) : LoginCredentialsFragmentFactory {

    override fun getFragment(intent: Intent): Fragment? {
        val accountType = intent.getStringExtra(LoginActivity.ACCOUNT_TYPE) ?: return null

        return when (accountType) {
            context.getString(R.string.eelo_account_type) -> getMurenaFragment(intent)
            context.getString(R.string.google_account_type) -> GoogleAuthFragment()
            context.getString(R.string.yahoo_account_type) -> YahooAuthFragment()
            else -> null
        }
    }

    private fun getMurenaFragment(intent: Intent): Fragment {
        val isAuthFlowComplete = intent.getBooleanExtra(LoginActivity.OPENID_AUTH_FLOW_COMPLETE, false)

        return when (isAuthFlowComplete) {
            true -> MurenaOpenIdAuthFragment()
            else -> EeloAuthenticatorFragment()
        }
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class MurenaLoginFlowFragmentModule {
    @Binds
    @IntoMap
    @IntKey(/* priority */ 30)
    abstract fun factory(impl: MurenaLoginFragmentFactory): LoginCredentialsFragmentFactory
}