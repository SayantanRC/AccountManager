/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.davdroid.ui.setup

import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import at.bitfire.davdroid.R

class InviteSuccessfulFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
        return inflater.inflate(R.layout.fragment_invite_successful, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val instructions = view.findViewById<TextView>(R.id.instructions)
        val formattedText = view.context.getText(R.string.instructions)
        instructions.text = formattedText
        view.findViewById<Button>(R.id.sign_in).setOnClickListener {
            try {
                activity?.let {
                    val accountManager = AccountManager.get(it)
                    val accountType = it.getString(R.string.eelo_account_type)
                    accountManager.addAccount(
                        accountType,
                        null,
                        null,
                        null,
                        it,
                        AccountManagerCallback<Bundle?> {
                            activity?.setResult(RESULT_OK, Intent())
                            activity?.finish()
                        },
                        null
                    )
                }
            } catch (e: Exception) {
            }
        }

        view.findViewById<Button>(R.id.sign_in_later).setOnClickListener {
            activity?.setResult(RESULT_OK, Intent())
            activity?.finish()
        }
    }
}