/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.os.Bundle
import android.text.Layout
import android.text.SpannableString
import android.text.style.AlignmentSpan
import android.view.View
import at.bitfire.davdroid.R
import at.bitfire.davdroid.authorization.IdentityProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.json.JSONObject

class GoogleAuthFragment : OpenIdAuthenticationBaseFragment(IdentityProvider.GOOGLE) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleConfirmationDialog()
    }

    override fun onAuthenticationComplete(userData: JSONObject) {
        val emailKey = "email"

        if (!userData.has(emailKey)) {
            handleLoginFailedToast()
            return
        }

        val email = userData.getString(emailKey)
        if (email.isBlank()) {
            handleLoginFailedToast()
            return
        }

        val baseUrl = "https://apidata.googleusercontent.com/caldav/v2/$email/user"
        proceedNext(email, baseUrl)
    }

    private fun handleConfirmationDialog() {
        if (isAuthFlowComplete()) {
            return
        }

        showConfirmationDialog()
    }

    private fun showConfirmationDialog() {
        val title = SpannableString(getString(R.string.warning))
        title.setSpan(
            AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
            0,
            title.length,
            0
        )

        MaterialAlertDialogBuilder(requireContext(), R.style.CustomAlertDialogStyle)
            .setTitle(title)
            .setMessage(R.string.google_alert_message)
            .setCancelable(false)
            .setPositiveButton(R.string.ok) { _, _ ->
                startAuthFLow()
            }.show()
    }
}
