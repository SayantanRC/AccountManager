/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.app.Application
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import androidx.annotation.WorkerThread
import androidx.lifecycle.AndroidViewModel
import at.bitfire.davdroid.OpenIdUtils
import at.bitfire.davdroid.authorization.IdentityProvider
import at.bitfire.davdroid.log.Logger
import net.openid.appauth.AppAuthConfiguration
import net.openid.appauth.AuthState
import net.openid.appauth.AuthState.AuthStateAction
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationRequest
import net.openid.appauth.AuthorizationResponse
import net.openid.appauth.AuthorizationService
import net.openid.appauth.AuthorizationService.TokenResponseCallback
import net.openid.appauth.AuthorizationServiceConfiguration
import net.openid.appauth.ClientSecretBasic
import net.openid.appauth.ResponseTypeValues
import net.openid.appauth.TokenResponse
import okio.buffer
import okio.source
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.logging.Level

class OpenIdAuthenticationViewModel(application: Application) : AndroidViewModel(application) {

    private val authorizationService: AuthorizationService
    private var authState: AuthState? = null
    var identityProvider: IdentityProvider? = null

    init {
        val appAuthConfig = AppAuthConfiguration.Builder()
            .setBrowserMatcher {
                it.useCustomTab
            }
            .build()

        authorizationService = AuthorizationService(getApplication(), appAuthConfig)
    }

    override fun onCleared() {
        authorizationService.dispose()
        super.onCleared()
    }

    fun getAuthState(): AuthState {
        return authState!!
    }

    fun setUpAuthState(intent: Intent): OpenIdAuthStateSetupState {
        if (authState != null) {
            return OpenIdAuthStateSetupState.ALREADY_SET_UP
        }

        val authStateString =
            intent.getStringExtra(LoginActivity.AUTH_STATE)
                ?: return OpenIdAuthStateSetupState.SET_UP_FAILED

        authState = AuthState.jsonDeserialize(authStateString)

        if (authState == null) {
            return OpenIdAuthStateSetupState.SET_UP_FAILED
        }

        return OpenIdAuthStateSetupState.SET_UP_SUCCEED
    }

    fun requestAuthCode(
        serviceConfiguration: AuthorizationServiceConfiguration,
        intent: Intent
    ) {
        authState = AuthState(serviceConfiguration)

        val loginHint = intent.getStringExtra(LoginActivity.USERNAME_HINT)

        val authRequest = AuthorizationRequest.Builder(
            serviceConfiguration,
            identityProvider!!.clientId,
            ResponseTypeValues.CODE,
            identityProvider!!.redirectUri
        )
            .setScope(identityProvider!!.scope)
            .setLoginHint(sanitizeHint(loginHint))
            .build()

        authorizationService.performAuthorizationRequest(
            authRequest,
            createPostAuthorizationIntent(authRequest, intent),
            authorizationService.createCustomTabsIntentBuilder().build()
        )
    }

    private fun sanitizeHint(hint: String?): String? {
        return if (hint.isNullOrBlank()) null else hint
    }

    private fun createPostAuthorizationIntent(
        request: AuthorizationRequest,
        providedIntent: Intent
    ): PendingIntent {
        val intent = Intent(getApplication(), LoginActivity::class.java)

        intent.putExtra(LoginActivity.AUTH_STATE, authState?.jsonSerializeString())

        intent.putExtra(
            LoginActivity.ACCOUNT_TYPE,
            providedIntent.getStringExtra(LoginActivity.ACCOUNT_TYPE)
        )
        intent.putExtra(LoginActivity.OPENID_AUTH_FLOW_COMPLETE, true)
        intent.putExtra(
            LoginActivity.OPEN_APP_PACKAGE_AFTER_AUTH,
            providedIntent.getStringExtra(LoginActivity.OPEN_APP_PACKAGE_AFTER_AUTH)
        )
        intent.putExtra(
            LoginActivity.OPEN_APP_ACTIVITY_AFTER_AUTH,
            providedIntent.getStringExtra(LoginActivity.OPEN_APP_ACTIVITY_AFTER_AUTH)
        )

        var flag = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            flag = flag or PendingIntent.FLAG_MUTABLE
        }

        return PendingIntent.getActivity(getApplication(), request.hashCode(), intent, flag)
    }

    fun performTokenRequest(
        authorizationResponse: AuthorizationResponse,
        authorizationException: AuthorizationException?,
        callback: TokenResponseCallback
    ) {
        authState?.update(authorizationResponse, authorizationException)
        val request = authorizationResponse.createTokenExchangeRequest()

        if (identityProvider?.clientSecret != null) {
            val clientAuth = ClientSecretBasic(identityProvider!!.clientSecret!!)
            authorizationService.performTokenRequest(request, clientAuth, callback)
            return
        }

        authorizationService.performTokenRequest(request, callback)
    }

    @WorkerThread
    fun retrieveUserInfoFromServer(infoEndpoint: String, accessToken: String): JSONObject? {
        try {
            val userInfoEndpoint = URL(infoEndpoint)
            val conn = userInfoEndpoint.openConnection() as HttpURLConnection
            conn.setRequestProperty("Authorization", "Bearer $accessToken")
            conn.instanceFollowRedirects = false
            val response = conn.inputStream.source().buffer()
                .readString(StandardCharsets.UTF_8)
            conn.inputStream.close()
            return JSONObject(response)
        } catch (ex: Exception) {
            Logger.log.log(Level.SEVERE, "failed to retrieve userInfo", ex)
        }

        return null
    }

    fun getUserInfoEndpoint(): String? {
        var infoEndpoint = identityProvider?.userInfoEndpoint

        if (infoEndpoint == null) {
            val discovery =
                authState?.authorizationServiceConfiguration?.discoveryDoc ?: return null
            infoEndpoint = discovery.userinfoEndpoint.toString()
        }

        return infoEndpoint
    }

    fun retrieveAccountInfo(
        response: TokenResponse,
        exception: AuthorizationException?,
        callback: AuthStateAction
    ) {
        authState?.update(response, exception)

        val clientAuth = OpenIdUtils.getClientAuthentication(identityProvider?.clientSecret)
        authState?.performActionWithFreshTokens(authorizationService, clientAuth, callback)
    }
}
