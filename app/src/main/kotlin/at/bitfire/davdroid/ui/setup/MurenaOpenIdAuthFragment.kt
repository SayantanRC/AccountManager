/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.os.Bundle
import android.view.View
import at.bitfire.davdroid.ECloudAccountHelper
import at.bitfire.davdroid.authorization.IdentityProvider
import org.json.JSONObject

class MurenaOpenIdAuthFragment : OpenIdAuthenticationBaseFragment(IdentityProvider.MURENA) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!isAuthFlowComplete() && ECloudAccountHelper.alreadyHasECloudAccount(requireContext())) {
            ECloudAccountHelper.showMultipleECloudAccountNotAcceptedDialog(requireActivity())
            return
        }

        startAuthFLow()
    }

    override fun onAuthenticationComplete(userData: JSONObject) {
        val userNameKey = "username"

        if (!userData.has(userNameKey)) {
            handleLoginFailedToast()
            return
        }

        val userName = userData.getString(userNameKey)
        if (userName.isBlank()) {
            handleLoginFailedToast()
            return
        }

        proceedNext(userName, "${IdentityProvider.MURENA.baseUrl}$userName")
    }
}
