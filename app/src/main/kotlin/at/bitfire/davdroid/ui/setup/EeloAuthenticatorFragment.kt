/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.ui.setup

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import at.bitfire.davdroid.Constants
import at.bitfire.davdroid.ECloudAccountHelper
import at.bitfire.davdroid.R
import at.bitfire.davdroid.databinding.FragmentEeloAuthenticatorBinding
import at.bitfire.davdroid.db.Credentials
import at.bitfire.davdroid.ui.ShowUrlActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.net.URI

class EeloAuthenticatorFragment : Fragment() {

    private val model by viewModels<EeloAuthenticatorModel>()
    private val loginModel by activityViewModels<LoginModel>()

    private val toggleButtonCheckedKey = "toggle_button_checked"
    var toggleButtonState = false

    private lateinit var serverToggleButton: Button
    private lateinit var userIdEditText: TextInputEditText
    private lateinit var serverUrlEditTextLayout: TextInputLayout
    private lateinit var serverUrlEditText: TextInputEditText
    private lateinit var passwordEditText: TextInputEditText
    private lateinit var passwordHolder: View

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        handleNoNetworkAvailable()

        val v = FragmentEeloAuthenticatorBinding.inflate(inflater, container, false)
        v.lifecycleOwner = this
        v.model = model

        serverToggleButton = v.root.findViewById(R.id.server_toggle_button)
        userIdEditText = v.root.findViewById(R.id.urlpwd_user_name)
        serverUrlEditTextLayout = v.root.findViewById(R.id.urlpwd_server_uri_layout)
        serverUrlEditText = v.root.findViewById(R.id.urlpwd_server_uri)
        passwordEditText = v.root.findViewById(R.id.urlpwd_password)
        passwordHolder = v.root.findViewById(R.id.password_holder)

        passwordHolder.isVisible = !EeloAuthenticatorModel.enableOpenIdSupport

        serverToggleButton.setOnClickListener { expandCollapse() }

        v.root.findViewById<View>(R.id.sign_in).setOnClickListener { login() }

        val tfaButton = v.root.findViewById<View>(R.id.twofa_info_button)
        tfaButton.setOnClickListener { show2FAInfoDialog() }
        tfaButton.isVisible = !EeloAuthenticatorModel.enableOpenIdSupport

        userIdEditText.doOnTextChanged { text, _, _, _ ->
            val domain = computeDomain(text)
            if (domain.isEmpty()) {
                serverUrlEditTextLayout.hint = getString(R.string.login_server_uri)
            } else {
                serverUrlEditTextLayout.hint = getString(R.string.login_server_uri_custom, domain)
            }
        }

        // code below is to draw toggle button in its correct state and show or hide server url input field
        //add by Vincent, 18/02/2019
        if (savedInstanceState != null) {
            toggleButtonState = savedInstanceState.getBoolean(toggleButtonCheckedKey, false)
        }

        //This allow the button to be redraw in the correct state if user turn screen
        if (toggleButtonState) {
            serverToggleButton.setCompoundDrawablesWithIntrinsicBounds(null, null , ContextCompat.getDrawable(requireContext(), R.drawable.ic_expand_less), null)
            serverUrlEditTextLayout.visibility = View.VISIBLE
            serverUrlEditText.isEnabled = true
        } else {
            serverToggleButton.setCompoundDrawablesWithIntrinsicBounds(null, null , ContextCompat.getDrawable(requireContext(), R.drawable.ic_expand_more), null)
            serverUrlEditTextLayout.visibility = View.GONE
            serverUrlEditText.isEnabled = false
        }
        return v.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (model.blockProceedWithLogin()) {
            ECloudAccountHelper.showMultipleECloudAccountNotAcceptedDialog(requireActivity())
        }
    }

    override fun onResume() {
        super.onResume()

        if (requireActivity().intent.getBooleanExtra(LoginActivity.RETRY_ON_401, false) && switchUserName()) {
            // user wants to login with murena account, but most probably provided wrong accountId as email.
            // switching email is done, retry login
            login()
            requireActivity().intent.putExtra(LoginActivity.RETRY_ON_401, false) // disable retry option to mitigate infinite looping
        }
    }

    /***
     * user put wrong accountId for the first time (for ex: abc@murena.io instead of abc@e.email)
     * this method check provided email & switch to alternative email if possible so retry can be handled in bg
     * @return email field's value is successfully switched or not. If true, proceed to retry
      */
    @SuppressLint("SetTextI18n")
    private fun switchUserName(): Boolean {
       if (userIdEditText.text.toString().contains("@")) {
           val username = userIdEditText.text.toString().substringBefore("@")
           val dns = userIdEditText.text.toString().substringAfter("@")

           if (dns == Constants.E_SYNC_URL) {
               userIdEditText.setText(username + "@" + Constants.EELO_SYNC_HOST)
               return true
           }

           if (dns == Constants.EELO_SYNC_HOST) {
               userIdEditText.setText(username + "@" + Constants.E_SYNC_URL)
               return true
           }
       }

        return false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(toggleButtonCheckedKey, toggleButtonState)
        super.onSaveInstanceState(outState)
    }

    /**
     * murena.io account can have userName which is not email address
     * But, we want to have email as userName so that auth to services like Mail doesn't break.
     * This method check the provided userName if it is not email & server is https://murena.io;
     * then add `@murena.io` after the userName to make full email address.
     */
    @SuppressLint("SetTextI18n")
    private fun purifyUserName(serverUrl: String) {
        val providedUserName = userIdEditText.text.toString()

        if (!providedUserName.contains("@") && serverUrl == "https://${Constants.EELO_SYNC_HOST}") {
            userIdEditText.setText("$providedUserName@${Constants.EELO_SYNC_HOST}")
        }
    }

    private fun computeDomain(username: CharSequence?) : String {
        var domain = "https://${Constants.EELO_SYNC_HOST}"

        if (!username.isNullOrEmpty() && username.toString().contains("@")) {
            var dns = username.toString().substringAfter("@")
            if (dns == Constants.E_SYNC_URL) {
                dns = Constants.EELO_SYNC_HOST
            }
            domain = "https://$dns"
        }
        return domain
    }

    private fun handleNoNetworkAvailable() {
        if (isNetworkAvailable()) {
            return
        }

        Toast.makeText(context, R.string.no_internet_toast, Toast.LENGTH_LONG).show()
        requireActivity().finish()
    }

    private fun login() {
        handleNoNetworkAvailable()

        val handleOpenIdAuth = EeloAuthenticatorModel.enableOpenIdSupport && !toggleButtonState
        val userId = userIdEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (handleOpenIdAuth && userId.isNotBlank()) {
            requireActivity().intent.putExtra(LoginActivity.USERNAME_HINT, userIdEditText.text.toString())
            navigate(MurenaOpenIdAuthFragment())
        } else if (userId.isNotBlank() && password.isNotBlank() && validate()) {
            navigate(DetectConfigurationFragment())
        } else {
            Toast.makeText(context, R.string.invalid_credentials, Toast.LENGTH_LONG).show()
        }

    }

    private fun navigate(fragment: Fragment) {
        parentFragmentManager.beginTransaction()
            .replace(android.R.id.content, fragment, null)
            .addToBackStack(null)
            .commit()
    }

    // if user wants to login with murena account, add support to automated retry
    // if user in any case provide wrong accountId as email (ex: abc@murena.io instead of abc@e.email)
    private fun addSupportRetryOn401IfPossible(serverUrl: String) {
        if ("https://${Constants.EELO_SYNC_HOST}" == serverUrl) {
            requireActivity().intent.putExtra(LoginActivity.RETRY_ON_401, true)
        }
    }

    private fun validate(): Boolean {
        var valid = false

        var serverUrl = serverUrlEditText.text.toString()

        if (serverUrl.isEmpty()) {
            serverUrl = computeDomain(userIdEditText.text.toString())
            addSupportRetryOn401IfPossible(serverUrl)
        }

        purifyUserName(serverUrl)

        fun validateUrl() {

            model.baseUrlError.value = null
            try {
                val uri = getServerURI(serverUrl)
                if (uri.scheme.equals("http", true) || uri.scheme.equals("https", true)) {
                    valid = true
                    loginModel.baseURI = uri
                } else
                    model.baseUrlError.value = getString(R.string.login_url_must_be_http_or_https)
            } catch (e: Exception) {
                model.baseUrlError.value = e.localizedMessage
            }
        }

        when {

            model.loginWithUrlAndTokens.value == true -> {
                validateUrl()

                val userName = userIdEditText.text.toString()
                val password = passwordEditText.text.toString()

                if (loginModel.baseURI != null) {
                    valid = true
                    loginModel.credentials = Credentials(userName.lowercase(), password, null, null, loginModel.baseURI)
                }
            }

        }

        return valid
    }

    private fun getServerURI(serverUrl: String): URI {
        if (serverUrl.startsWith("https://${Constants.EELO_SYNC_HOST}")) {
            return URI(Constants.MURENA_DAV_URL)
        }

        return URI(serverUrl)
    }

    /**
     * Show/Hide panel containing server's uri input field.
     */
    private fun expandCollapse() {
        if (!toggleButtonState) {
            serverToggleButton.setCompoundDrawablesWithIntrinsicBounds(null, null , ContextCompat.getDrawable(requireContext(), R.drawable.ic_expand_less), null)
            serverUrlEditTextLayout.visibility = View.VISIBLE
            serverUrlEditText.isEnabled = true
            toggleButtonState = true

            passwordHolder.visibility = View.VISIBLE
        } else {
            serverToggleButton.setCompoundDrawablesWithIntrinsicBounds(null, null , ContextCompat.getDrawable(requireContext(), R.drawable.ic_expand_more), null)
            serverUrlEditTextLayout.visibility = View.GONE
            serverUrlEditText.isEnabled = false
            toggleButtonState = false

            if(!EeloAuthenticatorModel.enableOpenIdSupport) {
                return
            }

            passwordHolder.visibility = View.GONE
            passwordEditText.text?.clear()
        }
    }

    private fun show2FAInfoDialog() {
        MaterialAlertDialogBuilder(requireContext(), R.style.CustomAlertDialogStyle)
            .setView(R.layout.dialog_2fa_info)
            .setPositiveButton(R.string.view_more) { _, _ ->
                val intent = Intent(requireContext(), ShowUrlActivity::class.java)
                    .putExtra(ShowUrlActivity.URL, getString(R.string.url_2fa_info))
                startActivity(intent)
            }
            .setNegativeButton(R.string.later, null)
            .show()

    }
}
