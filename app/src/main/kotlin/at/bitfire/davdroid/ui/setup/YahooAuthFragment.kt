package at.bitfire.davdroid.ui.setup

import android.os.Bundle
import android.text.Layout
import android.text.SpannableString
import android.text.style.AlignmentSpan
import android.view.View
import at.bitfire.davdroid.R
import at.bitfire.davdroid.authorization.IdentityProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.json.JSONObject

class YahooAuthFragment : OpenIdAuthenticationBaseFragment(IdentityProvider.YAHOO) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleConfirmationDialog()
    }

    override fun onAuthenticationComplete(userData: JSONObject) {
        val emailKey = "email"

        if (!userData.has(emailKey)) {
            handleLoginFailedToast()
            return
        }

        val email = userData.getString(emailKey)
        if (email.isBlank()) {
            handleLoginFailedToast()
            return
        }

        val calDavUrl = "https://caldav.calendar.yahoo.com/dav/$email"
        val cardDavUrl = "https://carddav.address.yahoo.com/dav/$email"
        proceedNext(email, calDavUrl, cardDavUrl)
    }

    private fun handleConfirmationDialog() {
        if (isAuthFlowComplete()) {
            return
        }

        showConfirmationDialog()
    }

    private fun showConfirmationDialog() {
        val title = SpannableString(getString(R.string.warning))
        title.setSpan(
            AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
            0,
            title.length,
            0
        )

        MaterialAlertDialogBuilder(requireContext(), R.style.CustomAlertDialogStyle)
            .setTitle(title)
            .setMessage(R.string.yahoo_alert_message)
            .setCancelable(false)
            .setPositiveButton(R.string.ok) { _, _ ->
                startAuthFLow()
            }.show()
    }
}