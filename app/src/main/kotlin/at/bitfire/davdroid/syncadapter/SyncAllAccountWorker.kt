/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.syncadapter

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import at.bitfire.davdroid.log.Logger
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import java.util.concurrent.TimeUnit
import java.util.logging.Level

/** Right after login to the cloud account, there is no guarantee the sync setup is finished, so user's might not see the calendar, task, contact accounts setup.
 * To resolve this we need to try syncing for a specific time limit with a multiplying wait time.
 * We need to use OneTimeWorker instead of PeriodicTimeWorker, because PeriodicTimWorker requires >= 15 min between it's job. But in our case, the job delays are in seconds.
 **/
@HiltWorker
class SyncAllAccountWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParameters: WorkerParameters,
) : Worker(appContext, workerParameters) {

    companion object {
        private const val NAME = "sync-all-accounts"
        private const val WAIT_SEC = "wait-sec"
        private const val DEFAULT_WAIT_SEC: Long = 5
        private const val WAIT_LIMIT_IN_SEC: Long = 120

        fun enqueue(context: Context, waitSec: Long = DEFAULT_WAIT_SEC) {
            if (waitSec > WAIT_LIMIT_IN_SEC) {
                return
            }

            val data = Data.Builder()
            data.putLong(WAIT_SEC, waitSec)

            WorkManager.getInstance(context).enqueueUniqueWork(
                NAME, ExistingWorkPolicy.REPLACE,
                OneTimeWorkRequestBuilder<SyncAllAccountWorker>()
                    .setInitialDelay(
                        waitSec,
                        TimeUnit.SECONDS
                    )   // wait some time before sync all accounts
                    .setInputData(data.build())
                    .build()
            )
        }
    }

    override fun doWork(): Result {
        Logger.log.log(Level.FINE, "sync all accounts work started")

        val result = SyncUtils.syncAllAccounts(applicationContext)
        if (!result) {
            return Result.success()
        }

        enqueueNext()
        return Result.success()
    }

    private fun enqueueNext() {
        var waitSec = inputData.getLong(WAIT_SEC, DEFAULT_WAIT_SEC)

        if (waitSec < DEFAULT_WAIT_SEC) {
            waitSec = DEFAULT_WAIT_SEC
        }

        enqueue(applicationContext, waitSec * 2)
    }
}
