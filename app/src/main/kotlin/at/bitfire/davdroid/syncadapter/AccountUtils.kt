/***************************************************************************************************
 * Copyright © All Contributors. See LICENSE and AUTHORS in the root directory for details.
 **************************************************************************************************/

package at.bitfire.davdroid.syncadapter

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import at.bitfire.davdroid.R
import at.bitfire.davdroid.log.Logger
import at.bitfire.davdroid.settings.AccountSettings
import at.bitfire.davdroid.util.setAndVerifyUserData
import com.owncloud.android.lib.common.accounts.AccountUtils

object AccountUtils {

    /**
     * Creates an account and makes sure the user data are set correctly.
     *
     * @param context  operating context
     * @param account  account to create
     * @param userData user data to set
     *
     * @return whether the account has been created
     *
     * @throws IllegalArgumentException when user data contains non-String values
     * @throws IllegalStateException if user data can't be set
     */
    fun createAccount(
        context: Context,
        account: Account,
        userData: Bundle,
        password: String? = null
    ): Boolean {
        // validate user data
        for (key in userData.keySet()) {
            userData.get(key)?.let { entry ->
                if (entry !is String)
                    throw IllegalArgumentException("userData[$key] is ${entry::class.java} (expected: String)")
            }
        }

        // create account
        val manager = AccountManager.get(context)
        if (!manager.addAccountExplicitly(account, password, userData))
            return false

        // Android seems to lose the initial user data sometimes, so set it a second time if that happens
        // https://forums.bitfire.at/post/11644
        if (!verifyUserData(context, account, userData))
            for (key in userData.keySet())
                manager.setAndVerifyUserData(account, key, userData.getString(key))

        if (!verifyUserData(context, account, userData))
            throw IllegalStateException("Android doesn't store user data in account")

        return true
    }

    private fun verifyUserData(context: Context, account: Account, userData: Bundle): Boolean {
        val accountManager = AccountManager.get(context)
        userData.keySet().forEach { key ->
            val stored = accountManager.getUserData(account, key)
            val expected = userData.getString(key)
            if (stored != expected) {
                Logger.log.warning("Stored user data \"$stored\" differs from expected data \"$expected\" for $key")
                return false
            }
        }
        return true
    }

    fun getMainAccountTypes(context: Context) =
        listOf(
            context.getString(R.string.account_type),
            context.getString(R.string.eelo_account_type),
            context.getString(R.string.google_account_type),
            context.getString(R.string.yahoo_account_type)
        )

    fun getMainAccounts(context: Context): List<Account> {
        val accountManager = AccountManager.get(context)
        val accounts = mutableListOf<Account>()

        getMainAccountTypes(context)
            .forEach {
                accounts.addAll(accountManager.getAccountsByType(it))
            }

        return accounts
    }

    fun getAddressBookAccountTypes(context: Context) =
        listOf(
            context.getString(R.string.account_type_address_book),
            context.getString(R.string.account_type_eelo_address_book),
            context.getString(R.string.account_type_google_address_book),
            context.getString(R.string.account_type_yahoo_address_book)
        )

    fun getAddressBookAccounts(context: Context): List<Account> {
        val accountManager = AccountManager.get(context)
        val accounts = mutableListOf<Account>()

        getAddressBookAccountTypes(context)
            .forEach {
                accounts.addAll(accountManager.getAccountsByType(it))
            }

        return accounts
    }

    fun getOpenIdMainAccountTypes(context: Context) =
        listOf(
            context.getString(R.string.eelo_account_type),
            context.getString(R.string.google_account_type),
            context.getString(R.string.yahoo_account_type)
        )

    fun getOpenIdMainAccounts(context: Context): List<Account> {
        val accountManager = AccountManager.get(context)
        val accounts = mutableListOf<Account>()

        getOpenIdMainAccountTypes(context)
            .forEach {
                accounts.addAll(accountManager.getAccountsByType(it))
            }

        return accounts
    }

    fun getOwnCloudBaseUrl(baseURL: String): String {
        if (baseURL.contains("/remote.php")) {
            return baseURL.split("/remote.php")[0]
        }

        return baseURL
    }

    fun getAccount(context: Context, userName: String?, requestedBaseUrl: String?): Account? {
        if (userName == null || requestedBaseUrl == null) {
            return null
        }

        val baseUrl = getOwnCloudBaseUrl(requestedBaseUrl)

        val accountManager = AccountManager.get(context.applicationContext)

        val accounts = getMainAccounts(context)

        for(account in accounts) {
            val name = accountManager.getUserData(account, AccountSettings.KEY_USERNAME)
            if (name != userName) {
                continue
            }

            val url = accountManager.getUserData(account, AccountUtils.Constants.KEY_OC_BASE_URL)
            if (url != null && getOwnCloudBaseUrl(url) == baseUrl) {
                return account
            }
        }

        return null
    }
}
