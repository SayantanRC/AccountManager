/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.receiver

import android.accounts.AccountManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import at.bitfire.davdroid.settings.AccountSettings
import at.bitfire.davdroid.syncadapter.AccountUtils
import at.bitfire.davdroid.ui.signout.OpenIdEndSessionActivity
import at.bitfire.davdroid.util.AuthStatePrefUtils
import com.owncloud.android.lib.common.OwnCloudClientManagerFactory

class AccountRemovedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null || intent == null || intent.action != AccountManager.ACTION_ACCOUNT_REMOVED) {
            return
        }

        val accountName = getAccountName(context, intent) ?: return

        val ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton()
        ownCloudClientManager.removeClientForByName(accountName)

        clearOidcSession(
            intent = intent,
            context = context,
            accountName = accountName
        )
    }

    private fun clearOidcSession(
        intent: Intent,
        context: Context,
        accountName: String
    ) {
        intent.extras?.getString(AccountManager.KEY_ACCOUNT_TYPE)?.let { type ->
            val authStateString =
                AuthStatePrefUtils.loadAuthState(context, accountName, type) ?: return
            startOidcEndSessionActivity(
                context = context,
                authState = authStateString,
                accountType = type
            )
        }
    }

    private fun startOidcEndSessionActivity(
        context: Context,
        authState: String,
        accountType: String
    ) {
        val intent = Intent(context, OpenIdEndSessionActivity::class.java)
        intent.apply {
            putExtra(AccountSettings.KEY_AUTH_STATE, authState)
            putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }

        context.applicationContext.startActivity(intent)
    }

    private fun getAccountName(context: Context, intent: Intent): String? {
        val accountType = intent.extras?.getString(AccountManager.KEY_ACCOUNT_TYPE)
        if (accountType !in AccountUtils.getMainAccountTypes(context)) {
            return null
        }

        return intent.extras?.getString(AccountManager.KEY_ACCOUNT_NAME)
    }
}
