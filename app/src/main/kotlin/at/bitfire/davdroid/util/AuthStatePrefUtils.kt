/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.util

import android.accounts.Account
import android.content.Context
import android.content.SharedPreferences

object AuthStatePrefUtils {

    private const val AUTH_STATE_SHARED_PREF = "authStateShared_Pref"

    fun saveAuthState(context: Context, account: Account, value: String?) {
        val preferences = getSharedPref(context)
        preferences.edit()
            .putString(getKey(account), value)
            .apply()
    }

    fun loadAuthState(context: Context, name: String, type: String): String? {
        val preferences = getSharedPref(context)
        val key = getKey(name = name, type = type)
        val value = preferences.getString(key, "")
        val authState = if (value.isNullOrBlank()) null else value

        authState.let {
            preferences.edit()
                .remove(key)
                .apply()
        }

        return authState
    }

    private fun getSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(AUTH_STATE_SHARED_PREF, Context.MODE_PRIVATE)
    }

    private fun getKey(account: Account): String {
        return getKey(name = account.name,
            type = account.type)
    }

    private fun getKey(name: String, type: String): String {
        return "$name==$type"
    }
}
