/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.network

import android.accounts.Account
import android.content.Context
import at.bitfire.davdroid.R
import okhttp3.CookieJar

fun createCookieStore(context: Context, account: Account? = null): CookieJar {
    if (account == null) {
        return MemoryCookieStore()
    }

    val murenaAccountType = context.getString(R.string.eelo_account_type)
    val murenaAddressBookAccountType = context.getString(R.string.account_type_eelo_address_book)

    if (account.type in listOf(murenaAccountType, murenaAddressBookAccountType)) {
        return PersistentCookieStore(context, account)
    }

    return MemoryCookieStore()
}
