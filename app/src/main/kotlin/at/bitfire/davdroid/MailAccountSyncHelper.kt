/*
 * Copyright MURENA SAS 2022
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid

import android.content.ComponentName
import android.content.Context
import android.content.Intent

object MailAccountSyncHelper {

    private const val MAIL_PACKAGE = "foundation.e.mail"
    private const val MAIL_RECEIVER_CLASS = "com.fsck.k9.account.AccountSyncReceiver"
    private const val ACTION_PREFIX = "foundation.e.accountmanager.account."

    fun syncMailAccounts(applicationContext: Context?) {
        val intent = getIntent()
        intent.action = ACTION_PREFIX + "create"
        applicationContext?.sendBroadcast(intent)
    }

    private fun getIntent() : Intent {
        val intent = Intent()
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
        intent.component = ComponentName(MAIL_PACKAGE, MAIL_RECEIVER_CLASS)
        return intent
    }
}