/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package at.bitfire.davdroid.util

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class UserIdFetcherTest {

    @Test
    fun `test fetch with input without 'users' part`() {
        val url = "https://murena.io"
        val actual = UserIdFetcher.fetch(url)
        assertNull(actual)
    }

    @Test
    fun `test fetch with input ends with 'users'`() {
        val baseUrl = "https://murena.io/remote.php/dav/principals/users/"
        val actual = UserIdFetcher.fetch(baseUrl)
        assertNull(actual)
    }

    @Test
    fun `test fetch with input with 'users' part`() {
        val baseUrl = "https://murena.io/remote.php/dav/principals/users/user@e.email"
        val expected = "user@e.email"
        val actual = UserIdFetcher.fetch(baseUrl)
        assertEquals(expected, actual)
    }

    @Test
    fun `test fetch with input with 'users' part & ends with slash`() {
        val baseUrl = "https://murena.io/remote.php/dav/principals/users/user@e.email/"
        val expected = "user@e.email"
        val actual = UserIdFetcher.fetch(baseUrl)
        assertEquals(expected, actual)
    }
}
